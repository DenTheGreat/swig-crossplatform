import example
import unittest


class TestMethods(unittest.TestCase):
    def test_int(self):
        self.assertEqual(example.increment(1), 2)

    def test_double(self):
        self.assertEqual(example.increment_double(1.2), 2.4)

    def test_float(self):
        self.assertAlmostEqual(example.increment_float(2.1), 3.4)

    def test_str(self):
        self.assertEqual(example.get_str('Hello'), 'Hello')


if __name__ == "__main__":
    unittest.main()