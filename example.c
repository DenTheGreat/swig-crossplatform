#include "example.h"

int increment(int n) {
    return n + 1;
}

double increment_double(double n){
    return n + 1.2;
}

float increment_float(float n){
    return n + 1.3;
}

char* get_str(char * str){
    return str;
}

