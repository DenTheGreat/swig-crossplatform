%module example

%{
#define SWIG_FILE_WITH_INIT
#include "example.h"
%}

int increment(int n);
double increment_double(double n);
float increment_float(float n);
char* get_str(char * str);